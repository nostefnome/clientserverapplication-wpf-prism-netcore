﻿namespace TestTask.Api.Contract.Requests;

public class UpdateInfoCardRequest
{
    public Guid Id { get; set; }
    public string? Title { get; set; }
    public byte[]? Image { get; set; }
}
