﻿namespace TestTask.Api.Contract.Requests;

public class AddInfoCardRequest
{
    public string Title { get; set; }
    public byte[] Image { get; set; }
}