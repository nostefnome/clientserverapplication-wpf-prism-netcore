﻿using RestEase;
using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;

namespace TestTask.Api.Contract;

public interface IApiClient
{
    [Get("api/infoCards/getAll")]
    Task<IEnumerable<InfoCard>> GetAllCards();

    [Post("api/infoCards/add")]
    Task<InfoCard> Add([Body] AddInfoCardRequest request);

    [Put("api/infoCards/update")]
    Task Update([Body] UpdateInfoCardRequest request);

    [Delete("api/infoCards/remove")]
    Task Remove([Query] Guid id);

    [Get("api/imageCard/get/{image}")]
    Task<FileStream> GetImage([Path] string image);
}
