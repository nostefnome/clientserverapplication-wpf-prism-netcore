using FluentValidation.AspNetCore;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using TestTask.Api.Contract;
using TestTask.Api.Data.Repositories.ImageRepository;
using TestTask.Api.Data.Repositories.InfoCardRepository;
using TestTask.Api.Service.Services.ImageCardService;
using TestTask.Api.Service.Services.InfoCardService;
using TestTask.Api.Service.Utils;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseWindowsService();

AddConfig(builder.Services, builder.Configuration);

var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint(
    "/swagger/v1/swagger.json", "TestTask v1"));
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

await app.RunAsync();

void AddConfig(IServiceCollection services, IConfiguration configuration)
{
    services.AddControllers();
    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "TestTask.API",
            Version = "v1"
        });
        c.ExampleFilters();
        c.CustomSchemaIds(type => type.ToString());
        c.EnableAnnotations();
    });
    services.AddSwaggerExamples();

    services.AddFluentValidation(options =>
    {
        options.RegisterValidatorsFromAssemblyContaining<IApiClient>();
    });
    services.AddAutoMapper(typeof(AutoMapperProfile));

    services.AddScoped<IImageRepository>(services => 
        new ImageRepository(configuration.GetConnectionString("Images")));
    services.AddScoped<IInfoCardRepository>(servies => 
        new InfoCardRepository(configuration.GetConnectionString("InfoCards")));

    services.AddScoped<IInfoCardService, InfoCardService>();
    services.AddScoped<IImageCardService, ImageCardService>();
}