﻿using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;
using TestTask.Api.Data.Repositories.ImageRepository;
using TestTask.Api.Data.Repositories.InfoCardRepository;

namespace TestTask.Api.Service.Services.InfoCardService;

public class InfoCardService : IInfoCardService
{
    private readonly IImageRepository _imageRepository;
    private readonly IInfoCardRepository _infoCardRepository;

    public InfoCardService(
        IImageRepository imageRepository,
        IInfoCardRepository infoCardRepository)
    {
        _imageRepository = imageRepository;
        _infoCardRepository = infoCardRepository;
    }

    public async Task<IEnumerable<InfoCard>> GetAll()
    {
        return await _infoCardRepository.GetAll();
    }

    public async Task<InfoCard> Add(AddInfoCardRequest request)
    {
        var image = await _imageRepository.Add(request.Image);
        var infoCard = await _infoCardRepository.Add(new InfoCard
        {
            Title = request.Title,
            Image = BuildImageUrl(image),
        });

        return infoCard;
    }

    public async Task RemoveById(Guid id)
    {
        await _infoCardRepository.Remove(id);
    }

    public async Task Update(UpdateInfoCardRequest request)
    {
        var card = (await _infoCardRepository.GetAll())
            .First(card => card.Id == request.Id);

        if (request.Title != null)
        {
            card.Title = request.Title;
        }

        if (request.Image != null)
        {
            _imageRepository.Remove(Path.GetFileName(card.Image));

            var imageName = await _imageRepository.Add(request.Image);
            card.Image = BuildImageUrl(imageName);
        }

        await _infoCardRepository.Update(card);
    }

    public async Task<bool> Exists(Guid id)
    {
        var cards = await _infoCardRepository.GetAll();

        return cards.Any(card => card.Id == id);
    }

    private string BuildImageUrl(string image)
    {
        return $"https://localhost:44344/api/imageCard/get/{image}";
    }
}