﻿using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;

namespace TestTask.Api.Service.Services.InfoCardService;

public interface IInfoCardService
{
    Task<IEnumerable<InfoCard>> GetAll();
    Task<InfoCard> Add(AddInfoCardRequest request);
    Task Update(UpdateInfoCardRequest request);
    Task RemoveById(Guid id);

    Task<bool> Exists(Guid id);
}