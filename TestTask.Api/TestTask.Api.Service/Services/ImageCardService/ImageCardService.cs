﻿using TestTask.Api.Data.Repositories.ImageRepository;

namespace TestTask.Api.Service.Services.ImageCardService;

public class ImageCardService : IImageCardService
{
    private readonly IImageRepository _imageRepository;

    public ImageCardService(IImageRepository imageRepository)
    {
        _imageRepository = imageRepository;
    }

    public FileStream Get(string image)
    {
        return _imageRepository.Get(image);
    }
}
