﻿namespace TestTask.Api.Service.Services.ImageCardService;

public interface IImageCardService
{
    FileStream Get(string image);
}
