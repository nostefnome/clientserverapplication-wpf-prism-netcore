﻿namespace TestTask.Api.Service.Utils;

static class InfoCardUtility
{
    public static string GenerateJPGFileName()
    {
        return $"{Guid.NewGuid()}.jpg";
    }
}