﻿using AutoMapper;
using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;

namespace TestTask.Api.Service.Utils;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<AddInfoCardRequest, InfoCard>();
    }
}
