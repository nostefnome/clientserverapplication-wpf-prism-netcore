﻿using Microsoft.AspNetCore.Mvc;
using TestTask.Api.Service.Services.ImageCardService;

namespace TestTask.Api.Service.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ImageCardController : Controller
{
    private readonly IImageCardService _imageCardService;

    public ImageCardController(IImageCardService imageCardService)
    {
        _imageCardService = imageCardService;
    }

    [HttpGet("[action]/{image}")]
    public async Task<IActionResult> Get([FromRoute] string image)
    {
        var result = _imageCardService.Get(image);
        return File(result, "image/jpg");
    }
}
