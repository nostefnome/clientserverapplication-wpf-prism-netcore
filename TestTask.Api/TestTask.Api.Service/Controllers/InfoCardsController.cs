﻿using Microsoft.AspNetCore.Mvc;
using TestTask.Api.Contract.Requests;
using TestTask.Api.Service.Services.InfoCardService;

namespace TestTask.Api.Service.Controllers;

[Route("api/[controller]")]
[ApiController]
public class InfoCardsController : Controller
{
    private readonly IInfoCardService _infoCardService;

    public InfoCardsController(IInfoCardService infoCardService)
    {
        _infoCardService = infoCardService;
    }

    [HttpGet("[action]")]
    public async Task<IActionResult> GetAll()
    {
        var result = await _infoCardService.GetAll();
        return Ok(result);
    }

    [HttpPost("[action]")]
    public async Task<IActionResult> Add([FromBody] AddInfoCardRequest request)
    {
        var infoCard = await _infoCardService.Add(request);
        return Ok(infoCard);
    }

    [HttpPut("[action]")]
    public async Task<IActionResult> Update(UpdateInfoCardRequest request)
    {
        //TODO: move all validator to controller
        await _infoCardService.Update(request);
        return Ok();
    }

    [HttpDelete("[action]")]
    public async Task<IActionResult> Remove([FromQuery] Guid id)
    {
        if (!await _infoCardService.Exists(id))
        {
            return NotFound("Card does not exists");
        }

        await _infoCardService.RemoveById(id);
        return Ok();
    }
}
