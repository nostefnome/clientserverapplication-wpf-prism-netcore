﻿namespace TestTask.Api.Data.Utils;

internal static class DirectoryUtils
{
    public static bool EnsureDirecotryExists(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
            return false;
        }

        return true;
    }
}
