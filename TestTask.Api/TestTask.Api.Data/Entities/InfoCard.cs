﻿namespace TestTask.Api.Data.Entities;

public class InfoCard
{
    public Guid Id { get; set; }

    public string Title { get; set; }
    public string Image { get; set; }
}
