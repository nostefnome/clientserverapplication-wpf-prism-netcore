﻿using TestTask.Api.Data.Entities;

namespace TestTask.Api.Data.Repositories.InfoCardRepository;

public interface IInfoCardRepository
{
    Task<InfoCard> Add(InfoCard infoCard);
    Task<IEnumerable<InfoCard>> GetAll();
    Task Update(InfoCard infoCard);
    Task Remove(Guid id);
}
