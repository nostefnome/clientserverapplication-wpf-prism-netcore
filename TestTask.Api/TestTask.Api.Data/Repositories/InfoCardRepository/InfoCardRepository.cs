﻿using System.Text.Json;
using TestTask.Api.Data.Entities;
using TestTask.Api.Data.Utils;

namespace TestTask.Api.Data.Repositories.InfoCardRepository;

public class InfoCardRepository : IInfoCardRepository
{
    private readonly string _path;

    public InfoCardRepository(string path)
    {
        _path = path;

        //TODO: Move to a composition root
        var folderExited = DirectoryUtils
            .EnsureDirecotryExists(Path.GetDirectoryName(path));

        if (!folderExited)
        {
            WriteInfoCards(new List<InfoCard>());
        }
    }

    public async Task<InfoCard> Add(InfoCard infoCard)
    {
        var entity = new InfoCard
        {
            Id = Guid.NewGuid(),
            Image = infoCard.Image,
            Title = infoCard.Title,
        };
        var infoCards = await  ReadAllInfoCards();
        infoCards.Add(entity);

        await WriteInfoCards(infoCards);

        return entity;
    }

    public async Task<IEnumerable<InfoCard>> GetAll()
    {
        return await ReadAllInfoCards();
    }

    public async Task Remove(Guid id)
    {
        var infoCards = (await ReadAllInfoCards())
            .Where(card => card.Id != id)
            .ToList();

        await WriteInfoCards(infoCards);
    }

    public async Task Update(InfoCard infoCard)
    {
        var infoCards = (await ReadAllInfoCards())
            .Where(card => card.Id != infoCard.Id)
            .ToList();
        infoCards.Add(infoCard);

        await WriteInfoCards(infoCards);
    }

    private async Task<List<InfoCard>> ReadAllInfoCards()
    {
        var json = await File.ReadAllTextAsync(_path);
        return JsonSerializer.Deserialize<List<InfoCard>>(json);
    }

    private async Task WriteInfoCards(List<InfoCard> cards)
    {
        var json = JsonSerializer.Serialize(cards);
        await File.WriteAllTextAsync(_path, json);
    }
}
