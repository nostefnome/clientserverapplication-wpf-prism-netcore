﻿using TestTask.Api.Data.Utils;

namespace TestTask.Api.Data.Repositories.ImageRepository;

public class ImageRepository : IImageRepository
{
    private readonly string _path;

    public ImageRepository(string path)
    {
        _path = path;

        //TODO: Move to interface
        DirectoryUtils.EnsureDirecotryExists(path);
    }

    public async Task<string> Add(byte[] image)
    {
        var fileName = GenerateImageName();
        var filePath = Path.Combine(_path, fileName);

        using (var stream = new FileStream(filePath, FileMode.Create))
        {
            await stream.WriteAsync(image);
        }

        return fileName;
    }

    public FileStream Get(string file)
    {
        var fullPath = Path.Combine(_path, file);
        return File.OpenRead(fullPath);
    }

    public void Remove(string file)
    {
        var path = Path.Combine(_path, file);
        File.Delete(path);
    }

    private string GenerateImageName(Guid? id = null)
    {
        return $"{id ?? Guid.NewGuid()}.jpg";
    }
}
