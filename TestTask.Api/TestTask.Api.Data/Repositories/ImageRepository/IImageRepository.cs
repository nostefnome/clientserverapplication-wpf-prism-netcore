﻿namespace TestTask.Api.Data.Repositories.ImageRepository;

public interface IImageRepository
{
    FileStream Get(string file);
    Task<string> Add(byte[] image);
    void Remove(string file);
}
