﻿using AutoMapper;
using InfoCardModule.Models;
using Prism.Logging;
using Prism.Ioc;
using RestEase;
using Serilog;
using System.Windows;
using TestTask.Api.Contract;
using TestTask.Api.Data.Entities;
using TestTask.Client.Presentation.ErrorHandling;
using TestTask.Client.Presentation.Views;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Serilog.Core;

namespace TestTask.Client.Presentation
{
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(path: "logs.txt", encoding: Encoding.UTF8)
                .CreateLogger();

            base.OnStartup(e);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IApiClient>(() => new ApiClientErrorDecorator(
                RestClient.For<IApiClient>("https://localhost:44344"),
                Log.ForContext<ApiClientErrorDecorator>()));

            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<InfoCardModel, InfoCard>();
                cfg.CreateMap<InfoCard, InfoCardModel>();
            });
            var mapper = configuration.CreateMapper();

            containerRegistry.Register<IMapper>(() => mapper);
        }
    }
}
