﻿using Serilog;
using System;

namespace TestTask.Client.Presentation.Utils
{
    internal static class LoggerUtils
    {
        private const string MessageRemplate = "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}";

        public static void LogError(ILogger logger, Exception exception)
        {
            logger.Error(exception, MessageRemplate);
        }
    }
}
