﻿namespace TestTask.Client.Presentation.Utils
{
    public static class Regions
    {
        public const string InfoCardsList = "InfoCardsList";
        public const string InfoCardDetail = "InfoCardDetail";
        public const string UpdateInfoCard = "UpdateInfoCard";
        public const string InputInfoCard = "InputInfoCardRegion";
    }
}
