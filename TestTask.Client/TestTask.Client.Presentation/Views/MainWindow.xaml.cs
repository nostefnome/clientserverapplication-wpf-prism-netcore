﻿using InfoCardModule.ViewModels;
using InfoCardModule.Views;
using Prism.Ioc;
using Prism.Regions;
using System.Windows;
using TestTask.Client.Presentation.Utils;
using TestTask.Client.Presentation.ViewModels;

namespace TestTask.Client.Presentation.Views
{
    public partial class MainWindow : Window
    {
        private readonly IContainerExtension _container;

        public MainWindow(IContainerExtension container)
        {
            InitializeComponent();

            _container = container;
            Loaded += MainWindow_Loaded;
        }
        
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var regionManager = RegionManager.GetRegionManager(this);

            var listInfoCards = _container.Resolve<ListInfoCards>();
            var infoCardsDetail = _container.Resolve<InfoCardDetail>();
            var updateInfoCard = _container.Resolve<UpdateInfoCard>();
            var inputInfoCard = _container.Resolve<InputInfoCard>();

            regionManager.Regions[Regions.InfoCardsList]
                .Add(listInfoCards, null, true);
            regionManager.Regions[Regions.InfoCardDetail]
                .Add(infoCardsDetail, null, true);
            regionManager.Regions[Regions.UpdateInfoCard]
                .Add(updateInfoCard, null, true);
            regionManager.Regions[Regions.InputInfoCard]
                .Add(inputInfoCard, null, true);

            AddSelectedInfoCardChangedEvent((ListInfoCardsViewModel)listInfoCards.DataContext);
        }

        private void AddSelectedInfoCardChangedEvent(ListInfoCardsViewModel listInfoCards)
        {
            var context = DataContext as MainWindowViewModel;
            listInfoCards.SelectedInfoCardChanged += context.OnSelectedInfoCardChangedHandler;
        }
    }
}
