﻿using RestEase;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using TestTask.Api.Contract;
using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;
using TestTask.Client.Presentation.Utils;

namespace TestTask.Client.Presentation.ErrorHandling
{
    public class ApiClientErrorDecorator : IApiClient
    {
        private readonly IApiClient _decorate;
        private readonly ILogger _logger;

        public ApiClientErrorDecorator(IApiClient decorate, ILogger logger)
        {
            _decorate = decorate;
            _logger = logger;
        }

        public async Task<InfoCard> Add([Body] AddInfoCardRequest request)
        {
            try
            {
                return await ExecuteQuery(async () => await _decorate.Add(request));
            }
            catch (ApiException exception)
            {
                MessageBox.Show("Возникла ошибка при добавлении карточки!", "Ошибка сервера");
                LoggerUtils.LogError(_logger, exception);
            }

            return default;
        }

        public async Task<IEnumerable<InfoCard>> GetAllCards()
        {
            try
            {
                return await ExecuteQuery(async () => await _decorate.GetAllCards());
            }
            catch (ApiException exception)
            {
                MessageBox.Show("Возникла ошибка при получении карточек!", "Ошибка сервера");
                LoggerUtils.LogError(_logger, exception);
            }

            return default;
        }

        public async Task<FileStream> GetImage([Path] string image)
        {
            try
            {
                return await ExecuteQuery(async () => await _decorate.GetImage(image));
            }
            catch (ApiException exception)
            {
                MessageBox.Show("Возникла ошибка при получении картинки!", "Ошибка сервера");
                LoggerUtils.LogError(_logger, exception);
            }

            return default;
        }

        public async Task Remove([Query] Guid id)
        {
            try
            {
                await ExecuteCommand(async () => await _decorate.Remove(id));
            }
            catch(ApiException exception)
            {
                MessageBox.Show("Возникла ошибка при удалении карточки!", "Ошибка сервера");
                LoggerUtils.LogError(_logger, exception);
            }
        }

        public async Task Update([Body] UpdateInfoCardRequest request)
        {
            try
            {
                await ExecuteCommand(async() => await _decorate.Update(request));
            }
            catch(ApiException exception)
            {
                MessageBox.Show("Возникла ошибка при обновлении карточки!", "Ошибка сервера");
                LoggerUtils.LogError(_logger, exception);
            }
        }

        private async Task<TResult> ExecuteQuery<TResult>(Func<Task<TResult>> method)
        {
            try
            {
                return await method.Invoke();
            }
            catch (HttpRequestException exception)
            {
                LoggerUtils.LogError(_logger, exception);
                MessageBox.Show("Не удалось подключиться к серверу!", "Ошибка подключения к серверу");
            }
            catch (Exception exception) when (exception is not ApiException)
            {
                LoggerUtils.LogError(_logger, exception);
                MessageBox.Show("Возникла неизвестная ошибка!", "Неизвестная ошибка");
            }

            return default;
        }

        private async Task ExecuteCommand(Func<Task> method)
        {
            try
            {
                await method.Invoke();
            }
            catch (HttpRequestException exception)
            {
                LoggerUtils.LogError(_logger, exception);
                MessageBox.Show("Не удалось подключиться к серверу!", "Ошибка подключения к серверу");
            }
            catch (Exception exception) when (exception is not ApiException)
            {
                LoggerUtils.LogError(_logger, exception);
                MessageBox.Show("Возникла неизвестная ошибка!", "Неизвестная ошибка");
            }
        }
    }
}
