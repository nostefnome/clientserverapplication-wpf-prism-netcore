﻿using AutoMapper;
using InfoCardModule.Events;
using InfoCardModule.Models;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using TestTask.Api.Contract;
using TestTask.Api.Contract.Requests;
using TestTask.Api.Data.Entities;
using TestTask.Client.Presentation.Utils;

namespace TestTask.Client.Presentation.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IApiClient _client;
        private readonly IMapper _mapper;

        private ObservableCollection<InfoCardModel> _infoCards = new();
        public ObservableCollection<InfoCardModel> InfoCards
        {
            get { return _infoCards; }
            set { SetProperty(ref _infoCards, value); }
        }

        private InfoCardModel _selectedInfoCard;
        public InfoCardModel SelectedInfoCard
        {
            get { return _selectedInfoCard; }
            set { SetProperty(ref _selectedInfoCard, value); }
        }

        public MainWindowViewModel(
            IApiClient client,
            IMapper mapper,
            IEventAggregator eventAggregator)
        {
            _client = client;
            _mapper = mapper;

            eventAggregator
                .GetEvent<RemoveInfoCardEvent>()
                .Subscribe(OnRemoveInfoCardEvent);
            eventAggregator
                .GetEvent<InfoCardInputEvent>()
                .Subscribe(OnInfoCardInputEvent);
            eventAggregator
                .GetEvent<UpdateInfoCardEvent>()
                .Subscribe(OnUpdateInfoCard);

            LoadInfoCards();
        }

        public void OnSelectedInfoCardChangedHandler(InfoCardModel infoCard)
        {
            SelectedInfoCard = infoCard;
        }

        private async void OnUpdateInfoCard(InfoCardModel updatedInfoCard)
        {
            await _client.Update(new UpdateInfoCardRequest
            {
                Id = (Guid)updatedInfoCard.Id,
                Title = updatedInfoCard.Title == SelectedInfoCard.Title ? 
                    null : updatedInfoCard.Title,
                Image = updatedInfoCard.Image == SelectedInfoCard.Image ?
                    null : await File.ReadAllBytesAsync(updatedInfoCard.Image)
            });

            LoadInfoCards();
        }

        private async void OnInfoCardInputEvent(InfoCardModel obj)
        {
            await _client.Add(new AddInfoCardRequest
            {
                Title = obj.Title,
                Image = File.ReadAllBytes(obj.Image)
            });

            LoadInfoCards();
        }

        private async void OnRemoveInfoCardEvent(Guid id)
        {
            await _client.Remove(id);
            LoadInfoCards();
        }

        private async void LoadInfoCards()
        {
            var infoCards = (await _client.GetAllCards())?
                .Select(card => _mapper.Map<InfoCardModel>(card));

            InfoCards.Clear();

            if (infoCards != null)
            {
                foreach (var card in infoCards)
                {
                    InfoCards.Add(card);
                }
            }
            
            SelectedInfoCard = null;
        }
    }
}
