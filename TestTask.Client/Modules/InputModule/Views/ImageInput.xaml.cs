﻿using InputModule.ViewModels;
using Prism.Common;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace InputModule.Views
{
    public partial class ImageInput : UserControl
    {
        public ImageInput(IContainerExtension containers)
        {
            InitializeComponent();

            RegionContext.GetObservableContext(this).PropertyChanged += ImageInput_PropertyChanged;
        }

        private void ImageInput_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>)sender;
            var image = (string)context.Value;
            (DataContext as ImageInputViewModel).SelectedImage = image;
        }
    }
}
