﻿using Microsoft.Win32;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;

namespace InputModule.ViewModels
{
    public class ImageInputViewModel : BindableBase
    {
        private string _selectedImage;
        public string SelectedImage
        {
            get => _selectedImage;
            set { SetProperty(ref _selectedImage, value); }
        }

        public DelegateCommand SelectImageCommand { get; private set; }

        public event Action<string> ImageSelected;

        public ImageInputViewModel()
        {
            SelectImageCommand = new DelegateCommand(SelectImageCommandExecute);
        }

        private void SelectImageCommandExecute()
        {
            var dialog = new OpenFileDialog()
            {
                Title = "Select a picture",
                Filter = "JPG Images|*.jpg"
            };

            if (dialog.ShowDialog() == true)
            {
                SelectedImage = dialog.FileName;
                ImageSelected?.Invoke(dialog.FileName);
            }
        }
    }
}
