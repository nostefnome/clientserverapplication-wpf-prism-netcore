﻿using InfoCardModule.Models;
using InfoCardModule.Utils;
using InfoCardModule.ViewModels;
using Prism.Common;
using Prism.Ioc;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using TestTask.Api.Data.Entities;

namespace InfoCardModule.Views
{
    public partial class ListInfoCards : UserControl
    {
        private readonly IContainerExtension _container;

        public ListInfoCards(IContainerExtension container)
        {
            InitializeComponent();

            _container = container;

            RegionContext.GetObservableContext(this).PropertyChanged += ListInfoCards_PropertyChanged;
        }

        private void ListInfoCards_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>)sender;
            (DataContext as ListInfoCardsViewModel).InfoCards = (ObservableCollection<InfoCardModel>)context.Value;
        }
    }
}
