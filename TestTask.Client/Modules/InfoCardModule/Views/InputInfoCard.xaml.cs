﻿using InfoCardModule.Utils;
using InfoCardModule.ViewModels;
using InputModule.ViewModels;
using InputModule.Views;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace InfoCardModule.Views
{
    public partial class InputInfoCard : UserControl
    {
        private readonly IContainerExtension _container;

        public InputInfoCard(IContainerExtension container)
        {
            InitializeComponent();

            _container = container;
            Loaded += InputInfoCard_Loaded;
        }

        private void InputInfoCard_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var regionManager = RegionManager.GetRegionManager(this);
            var imageInput = _container.Resolve<ImageInput>();

            regionManager.Regions[Regions.ImageInput]
                .Add(imageInput, null, true);

            AddImageSelectedEvent((ImageInputViewModel)imageInput.DataContext);
        }

        private void AddImageSelectedEvent(ImageInputViewModel imageInput)
        {
            var context = DataContext as InputInfoCardViewModel;
            imageInput.ImageSelected += context.OnImageChangedHandler;
        }
    }
}
