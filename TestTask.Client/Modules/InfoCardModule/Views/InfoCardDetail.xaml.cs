﻿using InfoCardModule.Models;
using InfoCardModule.ViewModels;
using Prism.Common;
using Prism.Regions;
using System.Windows.Controls;
using TestTask.Api.Data.Entities;

namespace InfoCardModule.Views
{
    /// <summary>
    /// Interaction logic for InfoCardDetail
    /// </summary>
    public partial class InfoCardDetail : UserControl
    {
        public InfoCardDetail()
        {
            InitializeComponent();
            RegionContext.GetObservableContext(this).PropertyChanged += InfoCardDetail_PropertyChanged;
        }

        private void InfoCardDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>)sender;
            (DataContext as InfoCardDetailViewModel).InfoCard = (InfoCardModel)context.Value;
        }
    }
}
