﻿using InfoCardModule.Models;
using InfoCardModule.Utils;
using InfoCardModule.ViewModels;
using InputModule.ViewModels;
using InputModule.Views;
using Prism.Common;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace InfoCardModule.Views
{
    public partial class UpdateInfoCard : UserControl
    {
        private readonly IContainerExtension _container;

        public UpdateInfoCard(IContainerExtension container)
        {
            InitializeComponent();

            _container = container;

            RegionContext.GetObservableContext(this).PropertyChanged += UpdateInfoCard_PropertyChanged;
            Loaded += UpdateInfoCard_Loaded;
        }

        private void UpdateInfoCard_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var regionManager = RegionManager.GetRegionManager(this);
            var imageInput = _container.Resolve<ImageInput>();

            regionManager.Regions[Regions.ImageInput]
                .Add(imageInput, null, true);

            AddImageSelectedEvent((ImageInputViewModel)imageInput.DataContext);
        }

        private void UpdateInfoCard_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>)sender;
            var infoCard = (InfoCardModel)context.Value;
            (DataContext as UpdateInfoCardViewModel).InfoCard = (InfoCardModel)infoCard?.Clone();
        }

        private void AddImageSelectedEvent(ImageInputViewModel imageInput)
        {
            var context = DataContext as UpdateInfoCardViewModel;
            imageInput.ImageSelected += context.OnImageChangedHandler;
        }
    }
}
