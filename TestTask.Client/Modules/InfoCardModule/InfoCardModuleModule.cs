﻿using InfoCardModule.Utils;
using InfoCardModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace InfoCardModule
{
    public class InfoCardModuleModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var manager = containerProvider.Resolve<IRegionManager>();

            manager.RegisterViewWithRegion<InfoCardDetail>(Regions.InfoCardDetail);
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}