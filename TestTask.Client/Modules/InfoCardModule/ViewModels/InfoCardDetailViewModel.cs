﻿using InfoCardModule.Models;
using Prism.Mvvm;

namespace InfoCardModule.ViewModels
{
    public class InfoCardDetailViewModel : BindableBase
    {
        private InfoCardModel _infoCard;
        public InfoCardModel InfoCard
        {
            get { return _infoCard; }
            set { SetProperty(ref _infoCard, value); }
        }

        public InfoCardDetailViewModel()
        {

        }
    }
}
