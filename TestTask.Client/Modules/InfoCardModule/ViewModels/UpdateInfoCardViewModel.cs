﻿using InfoCardModule.Events;
using InfoCardModule.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;

namespace InfoCardModule.ViewModels
{
    public class UpdateInfoCardViewModel : BindableBase
    {
        private readonly IEventAggregator _aggregator;

        private InfoCardModel _infoCard;
        public InfoCardModel InfoCard
        {
            get { return _infoCard; }
            set 
            { 
                SetProperty(ref _infoCard, value);
                RaisePropertyChanged(nameof(CanSelectImage));
            }
        }

        public DelegateCommand RemoveInfoCardCommand { get; private set; }
        public DelegateCommand UpdateInfoCardCommand { get; private set; }

        public bool CanSelectImage
        {
            get { return InfoCard != null; }
        }

        public UpdateInfoCardViewModel(IEventAggregator aggregator)
        {
            _aggregator = aggregator;

            RemoveInfoCardCommand = new DelegateCommand(RemoveInfoCardExecute, CanRemoveExecute)
                .ObservesProperty(() => InfoCard.Id);
            UpdateInfoCardCommand = new DelegateCommand(UpdateInfoCardExecute, CanUpdateExecute)
                .ObservesProperty(() => InfoCard.Image)
                .ObservesProperty(() => InfoCard.Title);
        }

        public void OnImageChangedHandler(string image)
        {
            InfoCard.Image = image;
        }

        private bool CanUpdateExecute()
        {
            return InfoCard?.Image != null && InfoCard?.Title != null;
        }

        private bool CanRemoveExecute()
        {
            return InfoCard?.Id != null;
        }

        private void UpdateInfoCardExecute()
        {
            _aggregator
                .GetEvent<UpdateInfoCardEvent>()
                .Publish(InfoCard);
        }

        private void RemoveInfoCardExecute()
        {
            _aggregator
                .GetEvent<RemoveInfoCardEvent>()
                .Publish((Guid)InfoCard.Id);
        }
    }
}
