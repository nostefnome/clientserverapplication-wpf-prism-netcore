﻿using InfoCardModule.Events;
using InfoCardModule.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;

namespace InfoCardModule.ViewModels
{
    public class InputInfoCardViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;

        private InfoCardModel _infoCard = new();
        public InfoCardModel InfoCard
        {
            get { return _infoCard; }
            set { SetProperty(ref _infoCard, value); }
        }

        public DelegateCommand InputCommand { get; private set; }

        public InputInfoCardViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            InputCommand = new DelegateCommand(OnInfoCardInput, CanInputExecute)
                .ObservesProperty(() => InfoCard.Title)
                .ObservesProperty(() => InfoCard.Image);
        }

        private bool CanInputExecute()
        {
            return !string.IsNullOrWhiteSpace(InfoCard.Title) && InfoCard.Image != null;
        }

        private void OnInfoCardInput()
        {
            _eventAggregator
                .GetEvent<InfoCardInputEvent>()
                .Publish(InfoCard);
        }

        public void OnImageChangedHandler(string image)
        {
            InfoCard.Image = image;
        }
    }
}
