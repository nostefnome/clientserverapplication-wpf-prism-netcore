﻿using InfoCardModule.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;

namespace InfoCardModule.ViewModels
{
    public class ListInfoCardsViewModel : BindableBase
    {
        private ObservableCollection<InfoCardModel> _infoCards;
        public ObservableCollection<InfoCardModel> InfoCards
        {
            get { return _infoCards; }
            set { SetProperty(ref _infoCards, value); }
        }

        public DelegateCommand<object> InfoCardSelectedCommand { get; private set; }

        public event Action<InfoCardModel> SelectedInfoCardChanged;

        public ListInfoCardsViewModel()
        {
            InfoCardSelectedCommand = new DelegateCommand<object>(InfoCardSelectedCommandExecute);
        }

        /// <summary>
        /// Use object type instead of InfoCardModel because when selected item was deleted command try calls with selectedITemEventArgs parameter's type.
        /// </summary>
        private void InfoCardSelectedCommandExecute(object obj)
        {
            if (obj is InfoCardModel)
            {
                SelectedInfoCardChanged?.Invoke(obj as InfoCardModel);
            }
        }
    }
}
