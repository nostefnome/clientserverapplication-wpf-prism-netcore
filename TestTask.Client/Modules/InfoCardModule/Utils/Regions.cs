﻿namespace InfoCardModule.Utils
{
    public static class Regions
    {
        public const string InfoCardDetail = "InfoCardDetailRegion";
        public const string ListCards = "ListCardsRegion";
        public const string ImageInput = "ImageInputRegion";
    }
}
