﻿using Prism.Mvvm;
using System;

namespace InfoCardModule.Models
{
    public class InfoCardModel : BindableBase, ICloneable
    {
        private Guid? _id;
        public Guid? Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _image;
        public string Image
        {
            get { return _image; }
            set { SetProperty(ref _image, value); }
        }

        public object Clone()
        {
            return new InfoCardModel
            {
                Id = _id,
                Title = _title,
                Image = _image
            };
        }
    }
}
