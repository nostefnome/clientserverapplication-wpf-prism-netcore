﻿using InfoCardModule.Models;
using Prism.Events;
using TestTask.Api.Data.Entities;

namespace InfoCardModule.Events
{
    public class InfoCardInputEvent: PubSubEvent<InfoCardModel>
    {
    }
}
