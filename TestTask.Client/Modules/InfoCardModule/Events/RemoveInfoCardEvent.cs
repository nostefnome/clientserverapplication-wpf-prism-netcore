﻿using Prism.Events;
using System;

namespace InfoCardModule.Events
{
    public class RemoveInfoCardEvent: PubSubEvent<Guid>
    {
    }
}
