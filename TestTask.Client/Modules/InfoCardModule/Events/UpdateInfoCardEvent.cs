﻿using InfoCardModule.Models;
using Prism.Events;

namespace InfoCardModule.Events
{
    public class UpdateInfoCardEvent: PubSubEvent<InfoCardModel>
    {

    }
}
